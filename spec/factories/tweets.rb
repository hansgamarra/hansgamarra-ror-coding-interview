FactoryBot.define do
  factory :tweet do
    body { Faker::GreekPhilosophers.quote }
    created_at { Time.zone.now }
    user
  end
end
