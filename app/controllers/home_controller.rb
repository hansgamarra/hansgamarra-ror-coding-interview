class HomeController < ApplicationController
  def index
    @tweets = Tweet.includes(:user).distinct(:user_id).limit(20).order("created_at DESC")
  end
end
