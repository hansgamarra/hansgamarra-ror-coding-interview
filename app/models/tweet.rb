# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  belongs_to :user

  validates :body, length: { maximum: 180 }
  validate :unique_body_within_24_hours

  scope :not_affiliated, -> { joins(:user).where(user: { company_id: nil }) }

  private

  def unique_body_within_24_hours
    errors.add(:body, 'should be unique within 24 hours') if Tweet.exists?(user_id: self.user_id, body: self.body, created_at: 24.hours.ago..)
  end
end
